package problems.qbf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import problems.qbfpt.triples.ForbiddenTriplesBuilder;
import problems.qbfpt.triples.Triple;
import solutions.Solution;


/**
 * Class representing the inverse of the Quadractic Binary Function
 * ({@link QBF}), which is used since the GRASP is set by
 * default as a minimization procedure.
 * 
 * @author ccavellucci, fusberti
 */
public class QBF_Inverse_PTP extends QBF {

	private double penaltyWeight = 50.0;
	private ForbiddenTriplesBuilder ftBuilder;

	
	/**
	 * Constructor for the QBF_Inverse class.
	 * 
	 * @param filename
	 *            Name of the file for which the objective function parameters
	 *            should be read.
	 * @throws IOException
	 *             Necessary for I/O operations.
	 */
	public QBF_Inverse_PTP(String filename, Double penalty) throws IOException {
		super(filename);
		this.penaltyWeight = penalty;
		this.ftBuilder= new ForbiddenTriplesBuilder(size);

	}


	/* (non-Javadoc)
	 * @see problems.qbf.QBF#evaluate()
	 */
	@Override
	public Double evaluateQBF() {
		return -super.evaluateQBF() +  violationPenalty(-1, -1);
	}
	
	/* (non-Javadoc)
	 * @see problems.qbf.QBF#evaluateInsertion(int)
	 */
	@Override
	public Double evaluateInsertionQBF(int i) {	
		return -super.evaluateInsertionQBF(i) +  violationPenalty(i, -1);
	}
	
	/* (non-Javadoc)
	 * @see problems.qbf.QBF#evaluateRemoval(int)
	 */
	@Override
	public Double evaluateRemovalQBF(int i) {
		return -super.evaluateRemovalQBF(i) + violationPenalty(-1, i);
	}
	
	/* (non-Javadoc)
	 * @see problems.qbf.QBF#evaluateExchange(int, int)
	 */
	@Override
	public Double evaluateExchangeQBF(int in, int out) {
		return -super.evaluateExchangeQBF(in,out) + violationPenalty(in, out);
	}
	
	public Double violationPenalty(int in, int out) {
		double penaltyValue = 0.0;
		int x,y,z;
		List<Triple> ftriples = ftBuilder.build();
		Double[] resVar = new Double[size];
		System.arraycopy(variables, 0, resVar, 0, size);
		
		if(in  >= 0) resVar[in]  = 1.0;
		if(out >= 0) resVar[out] = 0.0;
		
		for (Triple ft: ftriples) {
			x = ft.getX() -1;
			y = ft.getY() -1;
			z = ft.getZ() -1;
			
			if (resVar[x] == 1 && resVar[y] == 1 && resVar[z] == 1) {
				penaltyValue += penaltyWeight;

			}
		}
		
		return penaltyValue;
	}

}
