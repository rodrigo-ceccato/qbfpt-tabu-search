package problems.qbfpt.solvers;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import metaheuristics.tabusearch.AbstractTS;
import problems.qbf.QBF_Inverse;
import problems.qbfpt.triples.ForbiddenTriplesBuilder;
import solutions.Solution;



/**
 * Metaheuristic TS (Tabu Search) for obtaining an optimal solution to a QBF
 * (Quadractive Binary Function -- {@link #QuadracticBinaryFunction}).
 * Since by default this TS considers minimization problems, an inverse QBF
 *  function is adopted.
 * 
 * @author ccavellucci, fusberti
 */
public class TS_QBFPT extends AbstractTS<Integer> {
	
	private ForbiddenTriplesBuilder ftBuilder;
	
	private final Integer fake = new Integer(-1);
	
	private boolean firstImproving = false;

	/*random number generator for this class*/
	static Random rng = new Random(0);

	
	/**
	 * Constructor for the TS_QBF class. An inverse QBF objective function is
	 * passed as argument for the superclass constructor.
	 * 
	 * @param tenure
	 *            The Tabu tenure parameter.
	 * @param iterations
	 *            The number of iterations which the TS will be executed.
	 * @param filename
	 *            Name of the file for which the objective function parameters
	 *            should be read.
	 * @throws IOException
	 *             necessary for I/O operations.
	 */
	public TS_QBFPT(Integer tenure, Integer iterations, String filename, String searchMethod) throws IOException {
		super(new QBF_Inverse(filename), tenure, iterations);
		this.ftBuilder= new ForbiddenTriplesBuilder(ObjFunction.getDomainSize());

		if (searchMethod == "firstImproving") this.firstImproving = true;


	}

	/* (non-Javadoc)
	 * @see metaheuristics.tabusearch.AbstractTS#makeCL()
	 */
	@Override
	public ArrayList<Integer> makeCL() {

		ArrayList<Integer> _CL = new ArrayList<Integer>();
		for (int i = 0; i < ObjFunction.getDomainSize(); i++) {
			Integer cand = new Integer(i);
			_CL.add(cand);
		}

		return _CL;

	}

	/* (non-Javadoc)
	 * @see metaheuristics.tabusearch.AbstractTS#makeRCL()
	 */
	@Override
	public ArrayList<Integer> makeRCL() {

		ArrayList<Integer> _RCL = new ArrayList<Integer>();

		return _RCL;

	}
	
	/* (non-Javadoc)
	 * @see metaheuristics.tabusearch.AbstractTS#makeTL()
	 */
	@Override
	public ArrayDeque<Integer> makeTL() {

		ArrayDeque<Integer> _TS = new ArrayDeque<Integer>(2*tenure);
		for (int i=0; i<2*tenure; i++) {
			_TS.add(fake);
		}

		return _TS;

	}

	/* (non-Javadoc)
	 * @see metaheuristics.tabusearch.AbstractTS#updateCL()
	 */
	@Override
	public void updateCL() {
		
		if (!this.incumbentSol.isEmpty()) {
			List<Integer> forbiddenValues = new ArrayList<>();
			Integer lastElem = this.incumbentSol.get(this.incumbentSol.size()-1);
			for (int i = 0; i < this.incumbentSol.size()-1; i++) {
				forbiddenValues.addAll(ftBuilder.getForbiddenValues(this.incumbentSol.get(i)+1, lastElem+1));
//				System.out.println("forbidden = "  + forbiddenValues);
			}
			for (Integer fv : forbiddenValues) {
				int index = CL.indexOf(fv-1);
				if (index >= 0) {
//					System.out.println("removed: " + CL.get(index));
					CL.remove(index);
					
				}
			}
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * This createEmptySol instantiates an empty solution and it attributes a
	 * zero cost, since it is known that a QBF solution with all variables set
	 * to zero has also zero cost.
	 */
	@Override
	public Solution<Integer> createEmptySol() {
		Solution<Integer> sol = new Solution<Integer>();
		sol.cost = 0.0;
		return sol;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * The local search operator developed for the QBF objective function is
	 * composed by the neighborhood moves Insertion, Removal and 2-Exchange.
	 */
	@Override
	public Solution<Integer> neighborhoodMove() {

		Double minDeltaCost;
		Integer bestCandIn = null, bestCandOut = null;

		minDeltaCost = Double.POSITIVE_INFINITY;
		updateCL();
		
		/* adds randomnessto first-improving
		 * by swaping a random element with the first
		 */
	    int rndIndex = rng.nextInt(CL.size());
	
        Integer temp = CL.get(rndIndex);
        CL.set(rndIndex, CL.get(0));
        CL.set(0, temp);
		
		
		// Evaluate insertions
		for (Integer candIn : CL) {
			Double deltaCost = ObjFunction.evaluateInsertionCost(candIn, incumbentSol);
			if (!TL.contains(candIn) || incumbentSol.cost+deltaCost < bestSol.cost) {
				if (deltaCost < minDeltaCost) {
					minDeltaCost = deltaCost;
					bestCandIn = candIn;
					bestCandOut = null;
					if(firstImproving) break;
				}
			}
		}
		// Evaluate removals
		for (Integer candOut : incumbentSol) {
			Double deltaCost = ObjFunction.evaluateRemovalCost(candOut, incumbentSol);
			if (!TL.contains(candOut) || incumbentSol.cost+deltaCost < bestSol.cost) {
				if (deltaCost < minDeltaCost) {
					minDeltaCost = deltaCost;
					bestCandIn = null;
					bestCandOut = candOut;
					if(firstImproving) break;
				}
			}
		}
		// Evaluate exchanges
		for (Integer candIn : CL) {
			for (Integer candOut : incumbentSol) {
				Double deltaCost = ObjFunction.evaluateExchangeCost(candIn, candOut, incumbentSol);
				if ((!TL.contains(candIn) && !TL.contains(candOut)) || incumbentSol.cost+deltaCost < bestSol.cost) {
					if (deltaCost < minDeltaCost) {
						minDeltaCost = deltaCost;
						bestCandIn = candIn;
						bestCandOut = candOut;
						if(this.firstImproving) break;
					}
				}
			}
		}
		// Implement the best non-tabu move
		TL.poll();
		if (bestCandOut != null) {
			incumbentSol.remove(bestCandOut);
			CL.add(bestCandOut);
			TL.add(bestCandOut);
		} else {
			TL.add(fake);
		}
		TL.poll();
		if (bestCandIn != null) {
			incumbentSol.add(bestCandIn);
			CL.remove(bestCandIn);
//			System.out.println(bestCandIn);
			TL.add(bestCandIn);
		} else {
			TL.add(fake);
		}
		
		// allows non-improving moves
		// if nothing would be done, 
		if(bestCandOut == null && bestCandIn == null && incumbentSol.size() != 1) {			
			int solRandomPos = rng.nextInt(incumbentSol.size()-1);
			Integer randCandOut = incumbentSol.get(solRandomPos);
			TL.poll();
			TL.add(randCandOut);
			CL.add(randCandOut);
			incumbentSol.remove(randCandOut);
			
	
			int CLRandomPos = rng.nextInt(CL.size()-1);
			Integer randCandIn = CL.get(CLRandomPos);
			TL.poll();
			CL.remove(randCandIn);
			TL.add(randCandIn);
			incumbentSol.add(randCandIn);

			//System.out.println(randCandOut + " is now TABU!");
			//System.out.println(incumbentSol.toString());
		}
		
		/* remove some tabos if we wiped out the whole incubent sol*/
		else if (bestCandOut == null && bestCandIn == null && incumbentSol.size() == 0) {
			TL.poll();
			TL.add(fake);
		}
		
		ObjFunction.evaluate(incumbentSol);
		
		return null;
	}


	/**
	 * A main method used for testing the TS metaheuristic.
	 * 
	 */
	public static void main(String[] args) throws IOException {

		long startTime = System.currentTimeMillis();

		TS_QBFPT tabusearch = new TS_QBFPT(100, 10000, "instances/qbf040", "firstImproving");

		Solution<Integer> bestSol = tabusearch.solve();
		

		System.out.println("maxVal = " + bestSol);
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Time = "+(double)totalTime/(double)1000+" seg");

	}

}
